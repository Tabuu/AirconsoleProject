﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    Still,
    JumpCharge,
    Jumping,
    Landing,
    Fell,
    Death
}

public enum Direction
{
    Up = 0,
    Down,
    Left,
    Right
}

public class Player : MonoBehaviour {

    public event Action 
        JumpEvent,
        DieEvent,
        AttackEvent;

    Vector2[] _directionVectors = new Vector2[]
    {
        new Vector2(0, 1),
        new Vector2(0, -1),
        new Vector2(-1, 0),
        new Vector2(1, 0)
    };

    PlayerState _playerState;
    Direction _direction;

    float _chargeTime = 0f;

    bool _onGround = false;

    [SerializeField]
    float
        _jumpSpeed = 1f,
        _jumpForce = 1f;

    float _jumpTimer;

    [SerializeField]
    KeyCode 
        _right = KeyCode.RightArrow, 
        _left = KeyCode.LeftArrow, 
        _jump = KeyCode.Space, 
        _attack = KeyCode.LeftShift;

	void Start ()
    {
        _jumpTimer = _jumpSpeed;
        _playerState = PlayerState.Still;

        JumpEvent += OnBounce;
        AttackEvent += OnAttack;
    }
	
	void Update ()
    {
        if (_playerState != PlayerState.JumpCharge)
        {
            BounceCheck();
        }

        if (Input.GetKey(_right))
        {
            _direction = Direction.Right;
        }
        else if (Input.GetKey(_left))
        {
            _direction = Direction.Left;
        }
        else
        {
            _direction = Direction.Up;
        }

        if (Input.GetKey(_jump))
        {
            _chargeTime += Time.deltaTime;
            _playerState = PlayerState.JumpCharge;
        }
        else if (Input.GetKeyUp(_jump))
        {
            _playerState = PlayerState.Jumping;
        }

        if (Input.GetKeyDown(_attack))
        {
            AttackEvent();
        }
    }

    void OnPlayerMove()
    {

    }

    float xvel = 0;
    void OnBounce()
    {
        Rigidbody2D rigiBody = gameObject.GetComponent<Rigidbody2D>();

        rigiBody.velocity = new Vector2(xvel, 0.01f); // addforce sometimes doesn't set it !0 the first frame so this is a fix.

        Vector2 force = rigiBody.velocity;

        xvel *= 0.2f;

        xvel += 10 * _directionVectors[(int)_direction].x;

        force.y = _jumpForce * (1 +  (_chargeTime > 1 ? 1 : _chargeTime));
        force.x = xvel;

        _chargeTime = 0f;
        rigiBody.AddForce(force);

        Debug.Log(rigiBody.velocity);
    }

    void OnAttack()
    {

    }

    public void BounceCheck()
    {      
        Rigidbody2D rigiBody = gameObject.GetComponent<Rigidbody2D>();
        if (rigiBody.velocity.y == 0)
            JumpEvent();
    }
}
